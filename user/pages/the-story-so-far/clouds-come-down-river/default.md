---
title: 'Clouds Come Down River'
---

# The Coming Storm

There was a celebration in the keep. The litch king that had been terrorizing the area around Caerbannog. With the Wand of Power in hand Sheriff Needle and his warriors retired to enjoy a moment of relaxation. 

Under the cover of night, and armada of ships drove up river to the depth of *Loch Coinín bán*. A flotilla was formed, and ships began to go ashore. 