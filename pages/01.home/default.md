---
title: Home
---

<div class="row" markdown=1 >
<div class="col-md-4" markdown=1>
![](00613.jpg?cropResize=300,300)
</div>
<div class="col-md-8" markdown=1>
__The Shire of Caerbannog__ is the Keene New Hampshire chapter of Amtgard. We meet at [Robin Hood Park on Roxbury St. and Robin Hood LN in Keene New Hampshire](https://goo.gl/maps/pRyr9T2bE6w) on Saturdays at 11 AM.

[Facebook Group](https://www.facebook.com/groups/amtgard.caerbannog)

[Rules of Play](https://www.facebook.com/groups/amtgard.caerbannog/686513298084814/)

[Group Bylaws](https://www.facebook.com/groups/amtgard.caerbannog/907394609330014/)

[Waiver](https://www.facebook.com/groups/amtgard.caerbannog/780734985329311/)
</div>
</div>

---

[google-maps width='100%' lat=42.935716 lng=-72.265186 zoom=17 scrollwheel=true panControl=true]
***Robinhood Park***

Home of Caerbannog
[/google-maps]
